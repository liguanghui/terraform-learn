output "myapp-vpc-id" {
  value = aws_vpc.myapp-vpc.id
}
output "myapp-subnet-id" {
  value = aws_subnet.myapp-subnet-1.id
}