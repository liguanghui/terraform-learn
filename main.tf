terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
provider "aws" {
  region = "ap-northeast-1"
}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    Name        = var.cidr_blocks[0].name
    environment = var.environment
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id                  = aws_vpc.myapp-vpc.id
  map_public_ip_on_launch = true
  cidr_block              = var.cidr_blocks[1].cidr_block
  availability_zone       = "ap-northeast-1c"
  tags = {
    Name = var.cidr_blocks[1].name
  }
}

data "aws_vpc" "existing-vpc" {
  default = true
}

resource "aws_subnet" "myapp-subnet-2" {
  vpc_id                  = data.aws_vpc.existing-vpc.id
  map_public_ip_on_launch = true
  cidr_block              = "172.31.100.0/24"
  availability_zone       = "ap-northeast-1c"
  tags = {
    Name = "subnet-2-default"
  }
}