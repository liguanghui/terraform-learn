environment = "development"
cidr_blocks = [
  {
    cidr_block = "10.0.0.0/16"
    name       = "dev-vpc"
  },
  {
    cidr_block = "10.0.20.0/24"
    name       = "dev-subnet"
  }
]